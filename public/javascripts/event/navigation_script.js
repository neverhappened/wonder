$(document).ready(function() {

	$("div.baseFrame").click(function() {
		$("div.baseFrame").addClass("inactive");
		$("div.upTab").removeClass("inactive");
		$("div#comebackButton").removeClass("inactive");
		$("div#horizontalBar").removeClass("inactive");
	});

	$("div#comebackButton").click(function() {
		$("div.baseFrame").removeClass("inactive");
		$("div.upTab").addClass("inactive");
		$("div#comebackButton").addClass("inactive");
		$("div#horizontalBar").addClass("inactive");
		$("div.thickRuler").addClass("inactive");
	});

	$("div#profile").click(function() {
		$("div#redRuler").removeClass("inactive");
		$("div#yellowRuler").addClass("inactive")
		$("div#blueRuler").addClass("inactive");

		// $("div#profileContent").css("display", "block");
		// $("div#betsContent").css("display", "none");
		// $("div#graphicsContent").css("display", "none");

		// $("div#profileTopContent").css("display", "block");
		// $("div#betsTopContent").css("display", "none");
		// $("div#graphicsTopContent").css("display", "none");
	});
	$("div#bets").click(function() {
		$("div#redRuler").addClass("inactive");
		$("div#yellowRuler").removeClass("inactive")
		$("div#blueRuler").addClass("inactive");
		// $("div#profileContent").css("display", "none");
		// $("div#betsContent").css("display", "block");
		// $("div#graphicsContent").css("display", "none");

		// $("div#profileTopContent").css("display", "none");
		// $("div#betsTopContent").css("display", "block");
		// $("div#graphicsTopContent").css("display", "none");
	});
	$("div#graphics").click(function() {
		$("div#redRuler").addClass("inactive");
		$("div#yellowRuler").addClass("inactive")
		$("div#blueRuler").removeClass("inactive");
		// $("div#profileContent").css("display", "none");
		// $("div#betsContent").css("display", "none");
		// $("div#graphicsContent").css("display", "block");

		// $("div#profileTopContent").css("display", "none");
		// $("div#betsTopContent").css("display", "none");
		// $("div#graphicsTopContent").css("display", "block");
	});
}); 

