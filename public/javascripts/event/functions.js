var profileClicked = function() {
	$("div#profileTab").addClass("red");
	$("div#betsTab").removeClass("yellow");
	$("div#graphicsTab").removeClass("blue");

	$("div#redRuler").removeClass("inactive");
	$("div#yellowRuler").addClass("inactive")
	$("div#blueRuler").addClass("inactive");

	$("div#profileTab div.tabBottom").addClass("dark");
	$("div#betsTab div.tabBottom").removeClass("dark");
	$("div#graphicsTab div.tabBottom").removeClass("dark");

	$("div#profileTopContent").removeClass("inactive");
	$("div#betsTopContent").addClass("inactive");
	$("div#graphicsTopContent").addClass("inactive");
};

var betsClicked = function() {
	$("div#profileTab").removeClass("red");
	$("div#betsTab").addClass("yellow");
	$("div#graphicsTab").removeClass("blue");

	$("div#redRuler").addClass("inactive");
	$("div#yellowRuler").removeClass("inactive")
	$("div#blueRuler").addClass("inactive");

	$("div#profileTab div.tabBottom").removeClass("dark");
	$("div#betsTab div.tabBottom").addClass("dark");
	$("div#graphicsTab div.tabBottom").removeClass("dark");

	$("div#profileTopContent").addClass("inactive");
	$("div#betsTopContent").removeClass("inactive");
	$("div#graphicsTopContent").addClass("inactive");
};

var graphicsClicked = function() {
	$("div#profileTab").removeClass("red");
	$("div#betsTab").removeClass("yellow");
	$("div#graphicsTab").addClass("blue");

	$("div#redRuler").addClass("inactive");
	$("div#yellowRuler").addClass("inactive")
	$("div#blueRuler").removeClass("inactive");

	$("div#profileTab div.tabBottom").removeClass("dark");
	$("div#betsTab div.tabBottom").removeClass("dark");
	$("div#graphicsTab div.tabBottom").addClass("dark");

	$("div#profileTopContent").addClass("inactive");
	$("div#betsTopContent").addClass("inactive");
	$("div#graphicsTopContent").removeClass("inactive");
};

var comebackButtonClicked = function() {
	$("div.highlightFrame").removeClass("inactive");
	$("div.upTab").addClass("inactive");
	$("div#comebackButton").addClass("inactive");
	$("div#horizontalBar").addClass("inactive");
	$("div.thickRuler").addClass("inactive");
	$("div.topContent").addClass("inactive");
};

var mainTabClicked = function() {
	$("div.highlightFrame").addClass("inactive");
	$("div.upTab").removeClass("inactive");
	$("div#comebackButton").removeClass("inactive");
	$("div#horizontalBar").removeClass("inactive");
};