$(document).ready(function() {

	$("div.highlightFrame").click(mainTabClicked);
	$("div#comebackButton").click(comebackButtonClicked);

	$("div#profile").click(profileClicked);
	$("div#bets").click(betsClicked);
	$("div#graphics").click(graphicsClicked);

	$("div#profileTab").click(profileClicked);
	$("div#betsTab").click(betsClicked);
	$("div#graphicsTab").click(graphicsClicked);
});