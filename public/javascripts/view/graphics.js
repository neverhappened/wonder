
var data = [
	{
		value: 20,
		color: "#E47674"
	},
	{
		value: 30,
		color: "#FFFE7D"
	},
	{
		value: 50,
		color: "#409AD3"
	}
];

var data2 = {
	labels : ["January","February","March","April","May","June","July"],
	datasets : [
		{
			fillColor : "rgba(220,220,220,0.5)",
			strokeColor : "rgba(220,220,220,1)",
			pointColor : "rgba(220,220,220,1)",
			pointStrokeColor : "#fff",
			data : [65,59,90,81,56,55,40]
		},
		{
			fillColor : "rgba(151,187,205,0.5)",
			strokeColor : "rgba(151,187,205,1)",
			pointColor : "rgba(151,187,205,1)",
			pointStrokeColor : "#fff",
			data : [28,48,40,19,96,27,100]
		}
	]
}

// var ctx = $("#canvas").getContext("2d");
// new Chart(ctx).Doughnut(data);
// new Chart(ctx).Line(data2);